package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.Iterator;

public class GameScreen implements Screen {

    private final Drop game;
    private OrthographicCamera camera;
    private Texture dropImage, img, butterflyNetImage;
    private Sound dropSound;
    private Music music;
    private Rectangle butterflyNet;
    private Vector3 touchPos;
    private Array<Rectangle> drops;
    private long lastDropTime;
    private int dropsGatchered;
    private int dropsGatchered2;
    private int speed = 200;
    private int lvlspeed;

    GameScreen(final Drop gam) {
        img = new Texture("12.jpg");

        this.game = gam;

        // создает камеру
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);

        touchPos = new Vector3();
        // загрузка изображений для звезды и сачка
        dropImage = new Texture("1111.png");
        butterflyNetImage = new Texture("31.png");


        // загрузка звукового эффекта падающей звезды и фоновой "музыки"
        dropSound = Gdx.audio.newSound(Gdx.files.internal("падение.mp3"));
        music = Gdx.audio.newMusic(Gdx.files.internal("Clem Leek - Day Four.mp3"));

        music.setLooping(true);
        music.play();

        // создается Rectangle для представления звезды
        butterflyNet = new Rectangle();
        butterflyNet.x = 400 - 32;
        butterflyNet.y = 20;
        butterflyNet.width = 64;
        butterflyNet.height = 64;

        // создаем массив звезд и возрождаем первую
        drops = new Array<Rectangle>();
        spawndrop();

    }

    private void spawndrop(){
        Rectangle drop = new Rectangle();
        drop.x = MathUtils.random(0, 800-64);
        drop.y = 480;
        drop.width = 64;
        drop.height = 64;
        drops.add(drop);
        lastDropTime = TimeUtils.nanoTime();
    }

    @Override
    public void render (float delta) {

        // сообщает камере, что нужно обновить матрицы.
        camera.update();

// сообщаем SpriteBatch о системе координат
        // визуализации указанных для камеры.
        game.batch.setProjectionMatrix(camera.combined);

// начитаем новую серию, рисуем сачок и звезды
        game.batch.begin();
        game.batch.draw(img, 0, 0);
        game.font.draw(game.batch, "Drops Collected: " + dropsGatchered, 0, 480);
        game.font.draw(game.batch, "Lvl speed:" + lvlspeed, 700, 480);
        game.batch.draw(butterflyNetImage, butterflyNet.x, butterflyNet.y);
        for (Rectangle drop: drops) game.batch.draw(dropImage, drop.x, drop.y);
        game.batch.end();

        // обработка пользовательского ввода
        if(Gdx.input.isTouched()){
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPos);
            butterflyNet.x = (int) (touchPos.x -64 / 2);
        }

        if(Gdx.input.isKeyPressed(Input.Keys.LEFT)) butterflyNet.x -= speed * Gdx.graphics.getDeltaTime();
        if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) butterflyNet.x += speed * Gdx.graphics.getDeltaTime();
        // убедимся, что сачок остается в пределах экрана
        if (butterflyNet.x < 0)
            butterflyNet.x = 0;
        if (butterflyNet.x > 800 - 64)
            butterflyNet.x = 800 - 64;
//проверяем нужно ли спавнить звезду
        if (TimeUtils.nanoTime() - lastDropTime > 1000000000) spawndrop();

        // движение звезды, удаляем все звезды выходящие за границы экрана
        // или те, что попали в сачок. Воспроизведение звукового эффекта
        // при попадании.
        Iterator<Rectangle> iter = drops.iterator();
        while (iter.hasNext()){
            Rectangle drop = iter.next();
            drop.y -= speed * Gdx.graphics.getDeltaTime();
            if (drop.y + 64 < 0) iter.remove();
            if (drop.overlaps(butterflyNet)){
                dropsGatchered++;
                dropsGatchered2++;
                dropSound.play();
                iter.remove();
            }
        }
        int newlvl = 5;
        if(dropsGatchered2 == newlvl) {
            dropsGatchered2 = 0;
            lvlspeed++;
            speed=speed+300;
        }
            System.out.println(speed);
        }


    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        dropImage.dispose();
        butterflyNetImage.dispose();
        dropSound.dispose();
        music.dispose();
    }

    @Override
    public void show() {
        music.play();
    }
}
