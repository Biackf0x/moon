package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Drop extends Game {

    SpriteBatch batch;
    BitmapFont font;

    @Override
    public void create() {
        //для отображения объектов на экране, текстур
        batch = new SpriteBatch();
        //отображ текста
        font = new BitmapFont();
        this.setScreen(new MainMenuScreen(this));

    }
    //отобразим create
    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        super.dispose();
        batch.dispose();
        font.dispose();
    }
}